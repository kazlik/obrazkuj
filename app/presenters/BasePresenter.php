<?php

namespace App\Presenters;

use Nette,
    Nette\Application\UI,
    App,
    App\Model,
    App\Model\obrazkuj,
    Nette\Database\Context;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var Context */
    protected $database;

  /** @persistent */
  public $locale;

  /** @var \Kdyby\Translation\Translator @inject */
  public $translator;

  /**
    * @inject
    * @var \App\Model\obrazkuj
    */
  public $obrazkuj;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    public function handleUpvote($id)
    {
        if($this->user->isLoggedIn()){
          $image = new \App\Components\Imaggge($this->database, $this->user->id);
          $image->getByHash($id);
          $image->upVote();
          if($this->isAjax()) {
              $this->template->image = $image;
              $this->redrawControl('imageBlock');
          } else {
              $this->redirect('Homepage:detail', ['id' => $id]);
          }
        } else {
            $this->redirect('Homepage:detail', ['id' => $id]);
        }

    }

    public function handleDownvote($id)
    {
        if($this->user->isLoggedIn()){
            $image = new \App\Components\Imaggge($this->database, $this->user->id);
            $image->getByHash($id);
            $image->downVote();
            if($this->isAjax()) {
                $this->template->image = $image;
                $this->redrawControl('imageBlock');
            } else {
                $this->redirect('Homepage:detail', ['id' => $id]);
            }
        } else {
            $this->redirect('Homepage:detail', ['id' => $id]);
        }
    }

    public function handleNovote($id)
    {
        if($this->user->isLoggedIn()){
            $image = new \App\Components\Imaggge($this->database, $this->user->id);
            $image->getByHash($id);
            $image->noVote();
            if($this->isAjax()) {
                $this->template->image = $image;
                $this->redrawControl('imageBlock');
            } else {
                $this->redirect('Homepage:detail', ['id' => $id]);
            }
        } else {
            $this->redirect('Homepage:detail', ['id' => $id]);
        }
    }

  public function handleLogout(){
    $this->user->logout();
    $this->flashMessage($this->translator->translate('o.message.logoutSuccess'));
    $this->redirect('Homepage:');
  }

  protected function createComponentLoginForm()
    {
        $form = new UI\Form;
        $form->addText('username');
        $form->addPassword('password');
        $form->addSubmit('send')
          ->setOmitted(true);
        $form->onSuccess[] = [$this, 'loginFormSucceeded'];
        return $form;
    }

}
