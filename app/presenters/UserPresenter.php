<?php

namespace App\Presenters;

use Nette,
    Nette\Utils\Strings,
    Nette\Application\UI\Form;


class UserPresenter extends BasePresenter
{
  public $formError;

  public function renderLogin(){
    $template = $this->template;

  }

  public function renderJoin(){
    $template = $this->template;
  }

  public function renderDetail($username, $show = 'posts'){
      $template = $this->template;
      $userDetail = new \App\Components\Ussser($this->database, $username);
      $image = new \App\Components\Imaggge($this->database, $this->user->id);
      $template->userDetail = $userDetail;
      $template->image = $image;
      $template->show = $show;
  }

  public function createComponentRegisterForm()
  {
      $form = new Form;
      $form->addText('username')
        ->addRule(Form::LENGTH, 'Uživatelské jméno musí mít být dlouhé %d - %d znaků', array(3, 32))
        ->setRequired();
      $form->addText('email')
        ->setType('email')
        ->addRule(Form::EMAIL, 'Zadejte platnou emailovou adresu')
        ->setRequired();
      $form->addPassword('password')
        ->addRule(Form::LENGTH, 'Heslo musí mít být dlouhé %d - %d znaků', array(6, 32))
        ->setRequired();
      $form->addPassword('passwordCheck')
        ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['password'])
        ->setRequired();
      $form->addCheckbox('termsAgree')
        ->addRule(Form::EQUAL, 'Je potřeba souhlasit s podmínkami', TRUE);
      $form->addSubmit('send');
      $form->onSuccess[] = array($this, 'registerFormSucceeded');
      return $form;
  }
  public function RegisterFormSucceeded(Form $form, $values)
  {
    if($this->obrazkuj->existsUser($values->username))
    {
      $form->addError($this->translator->translate('o.message.usernameAlreadyExists'));
      $this->formError = true;
    }
    if($this->obrazkuj->existsEmail($values->email))
    {
      $form->addError($this->translator->translate('o.message.emailAlreadyExists'));
      $this->formError = true;
    }
    if(!$this->formError){
      $this->obrazkuj->addUser($values->username, $values->email, $values->password);
    }
  }

  public function LoginFormSucceeded(Form $form, $values)
  {

    try {
			$this->getUser()->login(Strings::lower($values->username), $values->password);
      $this->flashMessage($this->translator->translate('o.message.loginSuccess'));
      $this->redirect('Homepage:');
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
			return;
		}

  }
}
