<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;

class HomepagePresenter extends BasePresenter
{
    public function actionThumbnail($name, $width, $height = null)
    {
        $wallPicture = new \App\Components\Thumb($name, $width, $height);
        $this->sendResponse( $wallPicture );
    }

    public function handleUploadInfo($timestamp = 0)
    {
        $this->invalidateControl('uploadInfo');
        $template = $this->template;
        $template->timestamp = $timestamp;
        $image = new \App\Components\Uppploader($this->database, $this->user->id, $timestamp);
        $template->uploaded = $image;
    }

    public function handleChangeUploadInfo($show, $timestamp)
    {
        $cookie = $this->getSession('uploadInfo');
        switch($show){
            case 'detail':
            case 'hotlink':
                $cookie->show = $show;
            break;
        }
        $this->handleUploadInfo($timestamp);
    }

    public function renderDefault()
    {
        $template = $this->template;
        $template->tests = new \App\Components\Tesssts($this->database);
        $template->uploadCookie = $this->getSession('uploadInfo');
    }

    public function renderGroup($id)
    {
      $template = $this->template;

      $images = new \App\Components\Imaggges($this->database, $this->user->id);
      $images->getGroup($id);
      $template->images = $images;

      /*if(!$images->exists){
        $this->error('Group not found');
      }*/
    }

    public function renderDetail($id)
    {
      $template = $this->template;
      $image = new \App\Components\Imaggge($this->database, $this->user->id);
      $template->image = $image;
      $image->getByHash($id);

      if(!$image->exists){
        $this->error('Image not found');
      }
    }

        public function renderLastest()
    {
      $this->setView('list');
      $template = $this->template;
      $images = new \App\Components\Imaggges($this->database, $this->user->id);
      $images->lastest();
      $template->images = $images;
    }

    protected function createComponentUploadForm()
    {
        $form = new UI\Form;
        $form->setTranslator($this->translator);

        $renderer = $form->getRenderer();
        $renderer->wrappers['pair']['container'] = 'div class="form-group"';
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['control']['container'] = NULL;
        $renderer->wrappers['control']['.password'] = 'form-control';
        $renderer->wrappers['control']['.file'] = 'form-control';
        $renderer->wrappers['control']['.submit'] = 'btn btn-success pull-right';
        $renderer->wrappers['control']['errorcontainer'] = 'span class="text-danger pull-right"';
        $renderer->wrappers['label']['container'] = NULL;
        $renderer->wrappers['label']['suffix'] = ':';

        $form->addUpload('file', 'o.title.chooseUploadImage');
        $form->addHidden('timestamp')
            ->setDefaultValue(date('U'));
        $form->addSubmit('send', 'o.btn.upload')
          ->setOmitted(TRUE);
        $form->onSuccess[] = [$this, 'uploadFormSucceeded'];
        return $form;
    }

    public function uploadFormSucceeded(UI\Form $form, $values)
    {
        $image = new \App\Components\Imaggge($this->database, $this->user->id);
        $image->upload($values);
        if($this->isAjax()) {
            $this->invalidateControl('uploadInfo');
        }else{
            $this->redirect('Homepage:detail', $image->detail->hash);
        }
    }

    protected function createComponentPasteUploadForm()
    {
        $form = new UI\Form;
        $form->addText('image');
        $form->addSubmit('send')
          ->setOmitted(TRUE);
        $form->onSuccess[] = [$this, 'pasteUploadFormSucceeded'];
        return $form;
    }

    public function pasteUploadFormSucceeded(UI\Form $form, $values)
    {
        $image = new \App\Components\Imaggge($this->database, $this->user->id);
        $image->pasteUpload($values->image);
        $this->redirect('Homepage:detail', $image->detail->hash);
    }

    protected function createComponentCommentForm()
    {
        $form = new UI\Form;
        $form->setTranslator($this->translator);


        $form->addTextarea('text', 'o.title.commentText')
          ->setRequired('o.error.required');
        $form->addSubmit('send', 'o.btn.send')
          ->setOmitted(true);
        $form->onSuccess[] = [$this, 'commentFormSucceeded'];
        return $form;
    }

    public function commentFormSucceeded(UI\Form $form, $values)
    {
        $id = $this->request->getParameters()['id'];
        $image = new \App\Components\Imaggge($this->database, $this->user->id);
        $image->getByHash($id);
        $image->addComment($values->text);
        $this->redirect('Homepage:detail', ['id' => $id]);
    }

}
