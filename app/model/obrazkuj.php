<?php //TODO: Delete it all!
namespace App\Model;

use Nette\Utils\Strings,
    Nette\Database\Context,
    Nette\Utils\Image,
    Nette\Security\Passwords,
    Hashids;

class obrazkuj
{
  /** @var Context */
  private $database;

  public function __construct(Context $database)
  {
    $this->database = $database;
  }
  //============================================================================
  //User

  //Checks if username exists
  public function existsUser($username, $id = null){
    if($id){
      return $this->database->table('user')
        ->where('login', Strings::lower($username))
        ->where('id <> ?', $id)
        ->count("*");
    }else{
      return $this->database->table('user')
        ->where('login', Strings::lower($username))
        ->count("*");
    }
  }

  //Checks if email exists
  public function existsEmail($email, $id = null){
    if($id){
      return $this->database->table('user')
        ->where('email', Strings::lower($email))
        ->where('id <> ?', $id)
        ->count("*");
    }else{
      return $this->database->table('user')
        ->where('email', Strings::lower($email))
        ->count("*");
    }
  }

  public function addUser($username, $email, $password){
    $this->database->table('user')
      ->insert([
        'username' => $username,
        'login' => Strings::lower($username),
        'password' => Passwords::hash($password),
        'email' => Strings::lower($email)
      ]);
    return $this->database->getInsertId();
  }

}