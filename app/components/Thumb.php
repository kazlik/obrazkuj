<?php
namespace App\Components;

use Nette;
use Nette\Utils\Image;

class Thumb extends Nette\Object implements Nette\Application\IResponse
{
    private $name;
    private $width;
    private $height;
    private $thumbFilename;
    private $pathOriginal;
    private $pathThumb;

    public function __construct($name, $width, $height = null)
    {
        $this->name = $name;
        $this->width = $width;
        $this->height = $height;
        $this->pathOriginal = 'i/' . $this->name;
        $this->thumbFilename = $name;
        $this->pathThumb = 'i/thumb/' . $this->width . ($this->height != null ? 'x' . $this->height : '') . '/' . $this->thumbFilename;
    }

    public function send( Nette\Http\IRequest $httpRequest, Nette\Http\IResponse $httpResponse )
    {

        if(!file_exists($this->pathThumb)){
            $image = Image::fromFile($this->pathOriginal);
            $image->resize($this->width, $this->height, Image::SHRINK_ONLY);
            $image->save($this->pathThumb);
            $image->send( Image::PNG );
        }else{
            header('Content-Type: image/jpeg');
            readfile($this->pathThumb);
        }
    }

}