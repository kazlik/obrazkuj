<?php
namespace App\Components;

use Nette\Utils\Strings,
    Nette\Database\Context,
    Nette\Utils\Image,
    Nette\Application\UI,
    Nette\Security\Passwords,
    Hashids;

class Ussser extends UI\Control
{
    /** @var Context */
    private $database;

    private $id;

    public $detail;

    public function __construct(Context $database, $username)
    {
        $this->database = $database;
        $this->id = $this->database->table('user')
          ->where('username', $username)
          ->fetch()
          ->id;
        $this->detail = $this->database->table('user')
            ->get($this->id);
    }

    public function countUserPosts()
    {
        return $this->database->table('public')
            ->where('image.user_id', $this->id)
            ->count('*');
    }

    public function countUserImages()
    {
        return $this->database->table('image')
            ->where('user_id', $this->id)
            ->where('visible', 1)
            ->count('*');
    }

    public function countUserComments()
    {
        return $this->database->table('comment')
            ->where('user_id', $this->id)
            ->count('*');
    }

    public function countUserUpvotes()
    {
        return $this->database->table('upvote')
            ->where('user_id', $this->id)
            ->where('upvote', 1)
            ->count('*');
    }

    public function getUserPosts()
    {
        return $this->database->table('public')
            ->where('image.user_id', $this->id)
            ->order('created_at DESC');
    }

    public function getUserImages()
    {
        return $this->database->table('image')
            ->where('user_id', $this->id)
            ->where('visible', 1)
            ->order('created_at DESC');
    }

    public function getUserComments()
    {
        return $this->database->table('comment')
            ->where('user_id', $this->id)
            ->order('created_at DESC');
    }

    public function getUserUpvotes(){
        return $this->database->table('upvote')
            ->where('user_id', $this->id)
            ->where('upvote', 1)
            ->order('created_at DESC');
    }


}