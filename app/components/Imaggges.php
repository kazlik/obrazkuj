<?php
namespace App\Components;

use Nette\Utils\Strings,
    Nette\Database\Context,
    Nette\Utils\Image,
    Nette\Application\UI,
    Nette\Security\Passwords,
    Hashids;

class Imaggges extends UI\Control
{

    /** @var Context */
    private $database;

    private $userId;
    private $timestamp;
    private $groupHash;

    private $ids;


    public function __construct(Context $database, $userId = null)
    {
        $this->database = $database;
        $this->userId = $userId;
    }

    public function get()
    {
        foreach($this->ids as $id)
        {
            $idNew = new \App\Components\Imaggge($this->database, $this->userId);
            $idNew->getById($id);
            $ids[] = $idNew;
        }
        return $ids;
    }

    public function lastest()
    {
        foreach($this->database->table('public')
            ->order('created_at DESC')
            ->order('id DESC')
            ->limit(10)
            as $row){
            $this->ids[] = $row->image_id;
        }
    }

    public function getGroup($hash)
    {
        $this->groupHash = $hash;
        foreach($this->database->table('group_image')
            ->where('group.hash', $hash)
            as $row){
            $this->ids[] = $row->image_id;
        }
    }

    public function getGroupDetail()
    {
        return $this->database->table('group')
            ->where('hash', $this->groupHash)
            ->fetch();
    }

}