<?php
namespace App\Components;

use Nette\Utils\Strings,
    Nette\Database\Context,
    Nette\Utils\Image,
    Nette\Application\UI,
    Nette\Security\Passwords,
    Hashids;

class Imaggge extends UI\Control
{
    /** @var Context */
    private $database;

    private $id;
    private $userId;
    private $hashImage;
    private $hashGroup;

    public $detail;
    public $link;
    public $hotlink;
    public $exists;
    public $comments;
    public $countComments;
    public $upvotes;
    public $isUpvoted;
    public $isDownvoted;
    public $isSocial;
    public $socialName;

    public function __construct(Context $database, $userId = null)
    {
        $this->database = $database;
        $this->userId = $userId;
        $this->hashImage = new Hashids\Hashids('obrazkuju', 6);
        $this->hashGroup = new Hashids\Hashids('obrazkujGroup', 6);
    }

    public function getById($id)
    {
        $query = $this->database->table('image')
            ->get($id);
        return $this->setData($query);
    }

    public function getByHash($hash)
    {
        $query = $this->database->table('image')
            ->where('hash', $hash)
            ->fetch();
        return $this->setData($query);
    }

    private function setData($query)
    {
        if(!$query){
            $this->exists = false;
            return false;
        }else{
            $this->exists = true;
        }
        $this->id = $query->id;
        $this->detail = $this->database->table('image')
            ->get($this->id);
        $this->link = $this->detail->hash . '.' . $this->detail->ext;
        $this->hotlink = '/i/' . $this->detail->hash . '.' . $this->detail->ext;
        $this->comments = $this->database->table('comment')
            ->where('image_id', $this->id)
            ->order('created_at DESC');
        $this->countComments = $this->database->table('comment')
            ->where('image_id', $this->id)
            ->order('created_at DESC')
            ->count('*');
        $this->upvotes = $this->countVotes();
        $this->isUpvoted = $this->database->table('upvote')
            ->where('image_id', $this->id)
            ->where('user_id', $this->userId)
            ->where('upvote', 1)
            ->count('*');
        $this->isDownvoted = $this->database->table('upvote')
            ->where('image_id', $this->id)
            ->where('user_id', $this->userId)
            ->where('upvote', 0)
            ->count('*');
        $this->isSocial = $this->database->table('public')
            ->where('image_id', $this->id)
            ->count('*');
        if($this->isSocial)
        $this->socialName = $this->database->table('public')
            ->where('image_id', $this->id)
            ->fetch()
            ->name;

        return $this->id;
    }

    private function countVotes()
    {
        $upvotes = $this->database->table('upvote')
            ->where('image_id', $this->id)
            ->where('upvote', 1)
            ->count('*');
        $downvotes = $this->database->table('upvote')
            ->where('image_id', $this->id)
            ->where('upvote', 0)
            ->count('*');
        $votes = $upvotes - $downvotes;
        $votes <= 0 ? $votes = null : $votes = $votes;
        return $votes;
    }

    public function noVote()
    {
        $this->database->table('upvote')
            ->where('image_id', $this->id)
            ->where('user_id', $this->userId)
            ->delete();
    }

    public function upVote()
    {
        $this->novote();
        return $this->database->table('upvote')
            ->insert(
                [
                    'image_id' => $this->id,
                    'user_id' => $this->userId,
                    'upvote' => 1
                ]
            );
    }

    public function downVote()
    {
        $this->noVote();
        return $this->database->table('upvote')
            ->insert(
                [
                    'image_id' => $this->id,
                    'user_id' => $this->userId,
                    'upvote' => 0
                ]
            );
    }

    public function addComment($text){
      return $this->database->table('comment')
          ->insert(
                [
                    'image_id' => $this->id,
                    'user_id' => $this->userId,
                    'text' => $text
                ]
          );
    }

    public function upload($values)
    {
        $file = $values->file;

        $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $file->name);

        $image = $this->database->table('image')
            ->insert([
              'name' => $file->name,
              'user_id' => $this->userId,
              'ext' => $ext
            ]);
        $hash = $this->hashImage->encode($image->id);
        $file->move('i/' . $hash . '.' . $ext);
        $image->update([
          'hash' => $hash
        ]);

        $tempExists = $this->database->table('tempgroup')
            ->where('timestamp', $values->timestamp)
            ->where('ip', $_SERVER["REMOTE_ADDR"])
            ->count('*');


        if($tempExists){
            $tempGroup = $this->database->table('tempgroup')
                ->where('timestamp', $values->timestamp)
                ->where('ip', $_SERVER["REMOTE_ADDR"])
                ->fetch();
            if(!$tempGroup->group_id){
                $group = $this->database->table('group')
                    ->insert([
                        'user_id' => $this->userId,
                    ]);
                $group->update([
                    'hash' => $this->hashGroup->encode($group->id)
                    ]);
                $tempGroup->update([
                    'group_id' => $group->id
                    ]);
                $this->database->table('group_image')
                    ->insert([
                        'group_id' => $group->id,
                        'image_id' => $tempGroup->image_id
                ]);
            }else{
                $group = $this->database->table('group')
                    ->get($tempGroup->group_id);
            }
            $this->database->table('group_image')
                ->insert([
                    'group_id' => $group->id,
                    'image_id' => $image->id
                ]);
        }else{
            $tempGroup = $this->database->table('tempgroup')
                ->insert([
                    'timestamp' => $values->timestamp,
                    'ip' => $_SERVER["REMOTE_ADDR"],
                    'image_id' => $image->id
                ]);
        }
        $this->getById($image->id);
        $this->database->table('image')
          ->where('hash IS NULL')
          ->delete();
    }

    public function pasteUpload($rawImage)
    {
        $rawImage = explode(',', $rawImage);
        $rawImage = $rawImage[1];
        $image = Image::fromString( base64_decode('R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=='));
        $image = $this->database->table('image')
            ->insert([
              'name' => 'Clipboard ' . date('Y-m-d G:i:s'),
              'user_id' => $this->userId,
              'ext' => 'png'
            ]);
        $hash = $this->hashImage->encode($image->id);
        $image->update([
          'hash' => $hash
        ]);
        $this->getById($image->id);

        $imageData = Image::fromString(base64_decode($rawImage));
        $imageData->save('i/' . $hash . '.png');
    }

}