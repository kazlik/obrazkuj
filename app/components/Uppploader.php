<?php
namespace App\Components;

use Nette\Utils\Strings,
    Nette\Database\Context,
    Nette\Utils\Image,
    Nette\Application\UI,
    Nette\Security\Passwords,
    Hashids;

class Uppploader extends UI\Control
{

    /** @var Context */
    private $database;

    private $userId;
    private $timestamp;
    private $groupHash;

    private $ids;


    public function __construct(Context $database, $userId, $timestamp)
    {
        $this->database = $database;
        $this->userId = $userId;
        $this->timestamp = $timestamp;
        $this->getIds();
    }

    public function get()
    {
        foreach($this->ids as $id)
        {
            $idNew = new \App\Components\Imaggge($this->database, $this->userId);
            $idNew->getById($id);
            $ids[] = $idNew;
        }
        return $ids;
    }

    public function tempGroup()
    {
        return $this->database->table('tempgroup')
          ->where('timestamp', $this->timestamp)
          ->where('ip', $_SERVER["REMOTE_ADDR"])
          ->fetch()
          ->group;
    }

    public function getIds()
    {

        $images = $this->database->table('tempgroup')
          ->where('timestamp', $this->timestamp)
          ->where('ip', $_SERVER["REMOTE_ADDR"])
          ->fetch();
        if($images){
            if($images->group_id){
                foreach($images->group
                    ->related('group_image')
                    as $row){
                    $this->ids[] = $row->image_id;
                }

            }else{
                $this->ids[] = $images->image->id;
            }
        }
    }
}