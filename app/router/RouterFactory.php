<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
    $vote = '[/<id>/<do>]';
		$router = new RouteList;
    $router[] = new Route('i/thumb/<width>[x<height>]/<name>', 'Homepage:thumbnail');
    $router[] = new Route('image/<id>[/<do>]', 'Homepage:detail');
    $router[] = new Route('gallery/<id>', 'Homepage:group');
    $router[] = new Route('lastest' . $vote, 'Homepage:lastest');
    $router[] = new Route('user/<username>[/<show>]', 'User:detail');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}

}
